<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Change Log](#change-log)
    - [Sun May 26 15:50:40 PDT 2019](#sun-may-26-155040-pdt-2019)
    - [Fri May 24 22:51:57 PDT 2019](#fri-may-24-225157-pdt-2019)
    - [Wed May 22 14:49:16 PDT 2019](#wed-may-22-144916-pdt-2019)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Change Log

## instructions

- [workflow](docs/workflow.md)
- `~/lektor/docs/workflow.md`

## 2020-01-23

* [x] migrated - this moved to linode  & shanenull.com

## Mon Sep 16 15:11:58 PDT 2019

- [x] v2 weather

## Mon Jul  8 22:55:38 PDT 2019

- [x] shell scrape dad joke of the day

## Thu Jun 27 22:16:33 DST 2019

- [x] dad jokes in blog posts
 
## Tue Jun 11 20:58:53 PDT 2019

- [x] alias "blog"
- [x] small changes to blog template
- [ ] merge request 

## Mon May 27 20:12:02 PDT 2019

- [x] utils/cli.sh now runs job_dotbackup 
- [x] utils/cli.sh now runs job_daily

## Sun May 26 15:50:40 PDT 2019
- [x] upload max's concert video 

## Fri May 24 22:51:57 PDT 2019

- [x] screencast for homepage
- [x] little tune ups 

## Wed May 22 14:49:16 PDT 2019

- [x] install tag plugin
- [x] install doctoc 
- [x] utils/cli.sh - added job_daily creates daily blog post template


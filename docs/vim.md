# vim

```
"A mapping to make a backup of the current file.
function! WriteBackup()
  let l:fname = expand('%:p') . '__' . strftime('%Y_%m_%d_%H.%M.%S')
    silent execute 'write' l:fname
      echomsg 'Wrote' l:fname
      endfunction
nnoremap <Leader>ba :<C-U>call WriteBackup()<CR>
```

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [keybindings](#keybindings)
    - [blogging](#blogging)
        - [zsh](#zsh)
        - [tmux](#tmux)
        - [vim](#vim)
        - [newsboat newsbeuter](#newsboat-newsbeuter)
        - [browsers](#browsers)
    - [hardware keybindings](#hardware-keybindings)
        - [keyboardio](#keyboardio)
        - [pixelbook](#pixelbook)
        - [windows os](#windows-os)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# keybindings

Keyindings are a big part of this mouseless workflow.

Keybindings are learned by doing pyshically not memorizing mentally.

Printing a paper cheatsheet is handy to reference.

Mirror Vim bindings in other apps whenever possible.

## blogging 

C stands for control key

### zsh

- escape enters vim mode in terminal
- web-search plugin allows using google or youtube in terminal
- tmux aliases attach, list, kill
	- ta
	- tl
	- tkss
	- q list panes
	- '_' space - swap views

### tmux

- C-b ) move sessions
- C-b-o move panes
- C-b o move cursor
- C-b-d detach
    - see zsh section for more tmux related bindings
- m to mark letter ' letter jump to it
- + chmod

### ranger

- m mark
- ' jump to mark

### vim

some of these require plugins

- gf opens file under cursor 
- jump cursor 
	- list :ju C-o C-i
- C-p fuzzy finder
- C-n nerdtree
- autocompletes C-x/f/o/p

```
nnoremap <Leader>gy :Goyo<CR>
nnoremap <Leader>gh :r !date<CR>
nnoremap <Leader>gj :r !calendar -A 0<CR>
nnoremap <Leader>g; :r !date +\%j<CR>
nnoremap <Leader>g' :r !date +\%V<CR>
```


### newsboat newsbeuter

- I have vim bindings here will update this section

### browsers

- vimium plugin
- there is one for firefox on android
- ?
- yy yank url put in blog


## hardware keybindings

### keyboardio

- too many to list here
- overall design is left had moves back right forward
- hjkl arrows
- macros
- space cadet makes modifiers dual function
- ergox layer has numpad on right and symbols on left
- mouse layer - moves mouse

### pixelbook 

settings > keyboard allows you to remap without software

- alt shift i - send feedback
- finder / caps lock to backspace 
- backpace to alt
- ctrl to escape
- alt to control

### windows os

sharp keys remap modifiers

- caps lock to back space 
- ctrl to escape
- alt to control

### refs


<https://gist.github.com/MohamedAlaa/2961058>

<http://tmuxp.git-pull.com/en/latest/about_tmux.html>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [todo](#todo)
    - [done](#done)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# todo

> content

- redo screencast monthly
 
> general


☞ add digraph bullets to template ☞ ✓ x
- [ ] blog bsd cal links - merge request 
	- branch is 1-new-features
- [ ] setup doctoc (windows / ubuntu)
- [ ] next screencast - monthly or as needed?
	- [ ] stream 1-new-features branch is merged
	- [ ] the real workflow is readme > todo > changelog?
	- [ ] add keybindings to the readme?
	- [ ] show zsh web-search google & youtube used with newsboat
- [ ] doctoc - setup <https://pre-commit.com/>
- [ ] termux - setup lektor & calendar 
- [ ] finish python nltk script to rate journals
- [ ] find a movie trailer rss or ? and add to job_weekly for weekends amc + 
- [ ] reorganize folders
	- try some layouts -date +%V for week so 2019/quarters/months/weeks/days or ?
- [ ] toc on blog posts - doctoc did not work 
- [ ] write app to fork some of calendar files and update them with videos
- [ ] syntax highlighting
- [ ] image thumbnails

## done

[CHANGELOG](CHANGELOG.md)




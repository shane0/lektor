<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [pixelbook instructions](#pixelbook-instructions)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# pixelbook instructions

npm and doctoc

- `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash`
- `source ~/.bashrc`
- `nvm --version`
- `nvm install 10`
- `npm --version`
- `npm install -g doctoc`

# workflow

current flow for simple changes

- make an issue with merge request
	- <https://gitlab.com/shane0/lektor/issues>
- update changelog

```
~/lektor/CHANGELOG.md
```

<https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/>

<https://docs.gitlab.com/ce/user/markdown.html>


	To reference an issue: #123
	To reference a MR: !123
	To reference a snippet $123

- branch merge push

```
git fetch origin
git checkout -b 1-new-features origin/1-new-features

git fetch origin
git checkout origin/master
git merge --no-ff 1-new-features

git push origin master
```

- merge and close the issue

	closes #1

[big instructions](https://about.gitlab.com/handbook/communication/#gitlab-workflow)

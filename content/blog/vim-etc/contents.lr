title: vim etc.
---
author: Shane Null
---
body:

[vim](https://www.vim.org/) is my favorite text editor for many reasons.

Vim has many amazing keybindings, once you learn these you can reuse them in other editors using vim plugins, the terminal, and browser.

* distraction free mode aka zen mode (goyo limelight)
* [vimium](https://vimium.github.io/)
* android firefox [vimium ff plugin](https://addons.mozilla.org/en-US/firefox/addon/vimium-ff/)
* [zsh terminal vim plugin](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/vi-mode)

Side note: typically I edit this blog in vim text editor but when I want to preview pages in the browser I use a vimium plugin which allows you to go mouseless and do things like press r to reload, etc.  This is a huge time saver.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wlR5gYd6um0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
---
pub_date: 2019-05-22
---
twitter_handle: shane_null
---
tags:

vim
zen
daily

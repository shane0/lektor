<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [local workflow](#local-workflow)
    - [local setup](#local-setup)
- [remote workflow](#remote-workflow)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

![Build Status](https://gitlab.com/pages/lektor/badges/master/build.svg)

---

A [Lektor] blog hosted on GitLab Pages.

<https://shane0.gitlab.io/lektor>

Forked from <https://gitlab.com/pages/lektor>


```
git clone git@gitlabcom:shane0/lektor
```
original 

## workflow

```mermaid
graph TD;
 cloud_shell-->tmux_session;
 tmux_session-->job_daily;
```

- [command line interface](utils/cli.sh)
- [keybinding cheatsheet](docs/keybindings.md)
- [termux session](lk_tmux.sh)
- [solarized theme](https://ethanschoonover.com/solarized/)
- [tags plugin](https://www.getlektor.com/plugins/lektor-tags/)

- [CHANGELOG](CHANGELOG.md)


1. `python3 -m venv venv`
1. `source venv/bin/activate`
1. `pip install lektor`
1. `git clone git@gitlab.com:shane0/lektor`
1. `cd lektor`
1.  [pixelbook instructions](docs/pixelbook.md)
1. android - todo: document this 
1. dotfiles, keybindings & aliases - todo: document this


## remote workflow


> gitLab CI happens after push

- build - conversts markdown files to static html pages
- deploy - html > gitlab pages 

[GitLab CI][ci], following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:2.7

pages:
  script:
  - pip install lektor
  - lektor build --output-path public
  artifacts:
    paths:
    - public
  only:
  - master
```

~/bujo/current/index.md

[ci]: https://about.gitlab.com/gitlab-ci/
[lektor]: https://www.getlektor.com/
[install]: https://www.getlektor.com/docs/installation/
[documentation]: https://www.getlektor.com/docs/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

import click
import glob
import os
import pyperclip
import datetime

CURRENT_DIRECTORY = os.getcwd()
ISODATE = datetime.date.today().strftime('%Y-%m-%d')
ISOFILE = ISODATE + '.md'
# TODO change to env var 
SUPPORT_FOLDER = '//support1/_Support/PCAnywhere/' 

@click.group()
def cli():
    pass

@cli.command('sup1')
@click.option('--name',prompt='customer name',
                default='{{cookiecutter.customer_name}}')
@click.option('--ticketno',prompt='ticket no',
                default='{{cookiecutter.ticket_no}}')
def find_support_folder(name,ticketno):
    """create ticket folder on support1"""
    search_string = name[0:3] + '*'
    matches = glob.glob((SUPPORT_FOLDER + search_string))
    if len(matches) > 1:
        click.echo('multiple matches found - prompting which to use')
        for m in matches:
            if click.confirm('%s' % m):
                ticket_folder= m + '/Data/' + ticketno
                ticket_folder = ticket_folder.replace('\\','/')
                click.echo(ticket_folder)
                if click.confirm('create folder?'):
                    os.mkdir(ticket_folder)
                    pyperclip.copy(ticket_folder)
    elif len(matches) == 1:
        ticket_folder= matches[0] + '/Data/' + ticketno
        ticket_folder = ticket_folder.replace('\\','/')
        click.echo('match - %s' % ticket_folder)
        if click.confirm('mkdir %s '% ticket_folder):
            os.mkdir(ticket_folder)
            ticket_folder = ticket_folder.replace('/','\\')
            click.echo(ticket_folder)
            pyperclip.copy(ticket_folder)
    else:
        click.echo('no matches found')
    

@cli.command('sub')
def sub_folder():
    """create subfolder with named todays date"""
    folderpath = os.path.join(CURRENT_DIRECTORY, ISODATE)
    filepath = os.path.join(CURRENT_DIRECTORY,ISODATE,ISOFILE)
    click.echo(folderpath)
    click.echo(filepath)
    if not os.path.exists(folderpath):
        os.mkdir(folderpath)
    if not os.path.exists(filepath):
        open(filepath, 'a').close()
    click.echo('folder for today created')



if __name__ == "__main__":
    cli()

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [shell boilerplate readme](#shell-boilerplate-readme)
  - [jobs](#jobs)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# shell boilerplate readme

cli.sh is my shell boilerplate, you can add jobs which call tasks

## jobs

`./cli.sh job_daily` - this job creates todays blog post for lektor

```
~/lektor/content/blog/
└── 143
    └── contents.lr
```
adds todays calendar history

`calendar -A 0`

#!/bin/bash
#cli.sh
# basic shell boilerplate

# add your parameters here

# jobname (required) & date (optional)
jobname=$1
rundate=$2

# set date (for logging not for the job rundate)
LOGDATE=`date '+%Y-%m-%d %H:%M:%S'`

# test
# DAYNUMBER=testdir
DAYNUMBER=$(date +%j)
PUBDATE=$(date +%Y-%m-%d)

# start of the script echo host / user / date
echo '----) cli.sh starting - ' $LOGDATE
echo $HOSTNAME
runuser=$(whoami)
echo $runuser

# add your helpers here

# show functions for console help
show_functions(){
# job help
echo 'function list'
echo 'functions starting with job_ are accepted jobnames'
declare -F
}

# exit if params are jacked
bad_parameters () {
    echo >&2 "$@"
    echo ' '
    echo '----) cli.sh ending - ' $LOGDATE
    show_functions
    exit 1
}

# add tasks here

task_folder(){
  echo '  ----> task_folder started '
  echo ' '
#  mkdir ~/blog/content/blog/2019
  mkdir ~/blog/content/blog/$DAYNUMBER
  echo 'passed param ' $1
  echo '  ----> task one ended '
  echo ' '
}

task_file(){
  echo '  ----> task one started '
  echo ' '
  echo 'i got passed ' $1
  echo  'touch ~/blog/content/blog/$DAYNUMBER/contents.lr'
  touch ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '  ----> task one ended '
  echo ' '
}

task_weather(){
  echo '  ----> task one started '
  echo ' '
  echo 'i got passed ' $1
  echo  'touch ~/blog/content/blog/$DAYNUMBER/contents.lr'
  curl wttr.in/Seattle_1.png > ~/blog/content/blog/$DAYNUMBER/weather.png
  echo '  ----> task one ended '
  echo ' '
}

# so lazy one day i'll config file this mess
task_content(){
  echo '  ----> task_content one started '
  echo ' '
  echo 'title: ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'Day ' $DAYNUMBER >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '---' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'pub_date: ' $PUBDATE >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '---' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'author: Shane Null' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '---' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'tags: journal' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '---' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'body:'  >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'CHANGELOG - 1st world problem of the day?' >>   ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '⚡ ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo "![]($DAYNUMBER.png)" >> ~/blog/content/blog/$DAYNUMBER/contents.lr
# CURL='/usr/bin/curl'
# RVMHTTP="https://icanhazadjoke.com"
# CURLARGS="-f -s -S -k"
# joke="$($CURL $CURLARGS $RVMHTTP)"
  echo '## dad joke of the day ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '[dadjoke](https://icanhazadjoke.com) ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo "" >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'going to scrape this with shell or maybe python ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  jk=$(wget -q https://icanhazdadjoke.com -q -O - | grep -o  '<p class="subtitle">.*</p>' | sed 's/<p class="subtitle">//g'  | sed 's/<\/p>//g')
  echo "$jk" >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo "[wttr.in](http://wttr.in/Seattle)" >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo "![](weather.png)" >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '## history for ' $PUBDATE  >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr

all=$(calendar -A 0 | cut -d ' ' -f3- | sed s"/[:=<>'., ]/+/g" | tr "++" "+")
url="https://www.google.com/search?q="
while read -r line; 
  do 
  echo "[$line]($url$line)" >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
done <<< "$all"
  #echo '```' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  #calendar -A 0 >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  #echo '```' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '## newsboat' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '> comments on todays history and news?' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo 'imagine if you had a time machine and a teleport and could combine this day with this day in history?'  >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '## meditation' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo '☯ notes on sam harris waking up day __' >> ~/blog/content/blog/$DAYNUMBER/contents.lr
  echo ' ' >> ~/blog/content/blog/$DAYNUMBER/contents.lr

  echo '  ----> task_content one ended '
  echo ' '
  ranger
}
# task for dotfile backups

task_dotbackup(){
  echo '  ---->  dotfile backup task  started '
  echo ' '
    #!/usr/bin/zsh
    # date for backup file names
    logdate=`date '+%y-%m-%d'`
    machine=$(hostname)
    runuser=$(whoami)
    daynumber=$(date +%j)

    echo $logdate
    echo $machine
    echo $runuser
    echo $daynumber

    # setup folders
    mkdir ~/dotfiles/backups/$machine
    mkdir ~/dotfiles/backups/$machine/$daynumber
    mkdir ~/dotfiles/backups/$machine/$daynumber/zsh
    mkdir ~/dotfiles/backups/$machine/$daynumber/vim
    mkdir ~/dotfiles/backups/$machine/$daynumber/news

    # zsh
    cp ~/.zshrc ~/dotfiles/backups/$machine/$daynumber/zsh/zshrc_$logdate
    cp ~/.zsh_history ~/dotfiles/backups/$machine/$daynumber/zsh/zsh_history_$logdate
    cp ~/.zsh_aliases ~/dotfiles/backups/$machine/$daynumber/zsh/zsh_aliases_$logdate

    # vim
    cp ~/.vimrc ~/dotfiles/backups/$machine/$daynumber/vim/vimrc_$logdate

    # newsboat beuter
    cp ~/.newsboat/config ~/dotfiles/backups/$machine/$daynumber/news/config
    cp ~/.newsboat/urls ~/dotfiles/backups/$machine/$daynumber/news/urls

      echo '  ----> task dotfile backup ended '
      echo ' '
}
# add jobs here
# jobs call multiple tasks typically
# daily bootstraps a blog post
job_daily(){
  echo ' ====> job_daily started'
  echo ' '
  echo $jobname ' is the jobname'
  echo $rundate ' is the rundate'
  echo 'job_daily' was passed $1
  task_folder $DAYNUMBER
  task_file 'create the contents.lr file'
  task_weather
  task_content
  echo ' ====> job_daily ended'
  echo ' '
}

# grab weather image

job_weather(){
  task_weather 
}
# dotbackup
job_dotbackup(){
  echo ' ====> job_dotbackup started'
  echo ' '
  echo $jobname ' is the jobname'
  echo $rundate ' is the rundate'
  task_dotbackup
  echo ' ====> job_dotbackup finished'
  echo ' '
}
# validation - runs with 1 or 2 parameters - echo usage &  exit
echo ' '
echo 'validating parameters '
echo ' '
# yes I know this piece is distastefull lol
[ "$#" -lt 1 ] && bad_parameters "usage is cli (jobname - required) (yyyy-mm-dd - optional), $# provided"
[ "$#" -gt 2 ] && bad_parameters "usage is cli (jobname - required) (yyyy-mm-dd - optional), $# parameters ($@) exceeds the maximum"
[ "$#" -ge 1 ] && [ "$#" -le 2 ]  && echo $@ && echo ' ' && echo "parameters are valid"

# if rundate is not set this automatically set rundate to today
[ "$#" -eq 1 ] && rundate=`date '+%Y-%m-%d %H:%M:%S'`  && echo 'setting date'

# run the job
# add your job_name here
echo 'validating job_name' $jobname
case $jobname in
	job_daily)
	echo ' running ' job_daily
	job_daily
;;
	job_weather)
	echo ' running ' job_daily
	job_weather
;;
	job_dotbackup)
	echo ' running ' job_dotbackup
	job_dotbackup
;;
*)
echo $jobname ' did not match the job list' && show_functions
;;
esac

echo ' '
echo '----) cli.sh ending - ' $LOGDATE

#!/bin/bash
doctoc --gitlab ~/lektor/README.md
doctoc --gitlab ~/lektor/CHANGELOG.md
doctoc --gitlab ~/lektor/docs/todo.md
doctoc --gitlab ~/lektor/docs/keybindings.md
